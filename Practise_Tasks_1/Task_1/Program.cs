﻿using System;

namespace FirstTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var vector1 = new Vector(6, 3);

            // First index of the vector1 is 3, so this kind of assignment is going to call IndexOutOfRangeException
            // vector1[0] = 2;

            var vector2 = new Vector(2, 4);

            // Those two vectors are different, because they have different intervals.
            Console.WriteLine($"vector1 == vector2? {vector1 == vector2}");

            var vector3 = new Vector(6, 3);

            // Let's check now.
            Console.WriteLine($"vector1 == vector3? {vector1 == vector3} \n");

            // Let's add some numbers to vectors and try to add and subtract them.
            // For this operation we should use vector1 and vector3, because they have the same interval.
            Random value = new Random();

            for(var i = vector1.FirstIndex; i<vector1.Count; i++)
            {
                vector1[i] = value.Next(10);
            }

            for (var i = vector3.FirstIndex; i < vector3.Count; i++)
            {
                vector3[i] = value.Next(10);
            }

            Console.WriteLine($"First vector: {vector1.ToString()}");
            Console.WriteLine($"Second vector: {vector3.ToString()}\n");
            Console.WriteLine($"Addition: {(vector1 + vector3).ToString()}");
            Console.WriteLine($"Subtraction: {(vector1 - vector3).ToString()}\n");

            // And the last one, let's check the multiplication by a scalar. 
            Console.WriteLine($"First vector multiplication by 5: {(5 * vector1).ToString()}");

            Console.ReadKey();
        }
    }
}