﻿using System;
using System.Text;
using FirstTask.Exceptions;

/*
 * This is my Vector class. Some example of using Vector you can find in Program.cs of this project.
 * P.S. Index interval - it's first index of array and addition of first index and count; [firstIndex, firstIndex+count]
 * 
 * Enjoy :) 
 */

namespace FirstTask
{
    public class Vector
    {
        private int[] _data;
        private int _firstIndex;
        private int _count;

        public int FirstIndex
        {
            get
            {
                return _firstIndex;
            }

            private set
            {
                _firstIndex = value;
            }
        }

        public int Count
        {
            get
            {
                return _count;
            }

            private set
            {
                _count = value;
            }
        }

        public Vector(int count, int firstIndex = 0)
        {
            Count = count;
            FirstIndex = firstIndex;
            _data = new int[FirstIndex + Count];
        }

        public int this[int index]
        {
            get
            {
                if ((index < FirstIndex) || (index >= Count + FirstIndex))
                {
                    throw new IndexOutOfRangeException();
                }

                return _data[index];
            }

            set
            {
                if ((index < FirstIndex) || (index >= Count + FirstIndex))
                {
                    throw new IndexOutOfRangeException();
                }

                _data[index] = value;
            }
        }

        public static Vector operator +(Vector vector1, Vector vector2)
        {
            if ((vector1.FirstIndex != vector2.FirstIndex) || (vector1.Count != vector2.Count))
            {
                throw new DifferentIndexIntervalException();
            }

            var result = new Vector(vector1.Count, vector1.FirstIndex);

            for(var i = result.FirstIndex; i < result.Count; i++)
            {
                result[i] = vector1[i] + vector2[i];
            }

            return result;
        }

        public static Vector operator -(Vector vector1, Vector vector2)
        {
            if ((vector1.FirstIndex != vector2.FirstIndex) || (vector1.Count != vector2.Count))
            {
                throw new DifferentIndexIntervalException();
            }

            var result = new Vector(vector1.Count, vector1.FirstIndex);

            for (var i = result.FirstIndex; i < result.Count; i++)
            {
                result[i] = vector1[i] - vector2[i];
            }

            return result;
        }

        public static Vector operator *(Vector vector, int scalar)
        {
            for(var i = vector.FirstIndex; i < vector.Count; i++)
            {
                vector[i] *= scalar;
            }

            return vector;
        }

        public static Vector operator *(int scalar, Vector vector)
        {
            return vector * scalar;
        }

        public static bool operator ==(Vector vector1, Vector vector2)
        {
            return vector1.Equals(vector2);
        }

        public static bool operator !=(Vector vector1, Vector vector2)
        {
            return !vector1.Equals(vector2);
        }

        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }

            if(GetType() != obj.GetType())
            {
                return false;
            }

            var vector = obj as Vector;

            if ((FirstIndex != vector.FirstIndex) || (Count != vector.Count))
            {
                return false;
            }

            for (var i = FirstIndex; i < Count; i++)
            {
                if (_data[i] != vector[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            var hashcode = Count;

            for(var i = 0; i<Count; i++)
            {
                hashcode = unchecked(hashcode * 31 + _data[i]);
            }

            return hashcode;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            for(var i = FirstIndex; i<Count; i++)
            {
                builder.Append(_data[i].ToString() + " ");
            }

            return builder.ToString();
        }
    }
}