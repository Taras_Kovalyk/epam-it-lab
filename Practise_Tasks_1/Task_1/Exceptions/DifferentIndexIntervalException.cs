﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstTask.Exceptions
{
    public class DifferentIndexIntervalException : Exception
    {
        public DifferentIndexIntervalException()
        {

        }

        public DifferentIndexIntervalException(string message) 
            : base (message)
        {

        }

        public DifferentIndexIntervalException(string message, Exception inner)
            : base (message, inner)
        {

        }
    }
}
