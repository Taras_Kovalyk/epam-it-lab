﻿using FourthTask.Classes;
using System;

namespace FourthTask
{
    class Program
    {
        static void Main(string[] args)
        {
            // Let's create some matrix;
            var matrix1 = new Matrix(4, 4)
            {
                {1, 3, 5, 3},
                {3, 2, 9, 1},
                {5, -7, 2, 9},
                {1, 3, -4, 0},
            };

            // To check matrix math operations, let's create another one.
            var matrix2 = new Matrix(4, 4)
            {
                {4, -4, 3, 5},
                {7, 0, -7, 1},
                {1, 5, 1, 2},
                {9, 1, 2, 3},
            };

            // Let's see what we got.
            Console.WriteLine("First matrix:");
            Console.WriteLine(matrix1.ToString());

            Console.WriteLine("Second matrix:");
            Console.WriteLine(matrix2.ToString());

            // Let's get a matrix n-rank minor.
            Console.WriteLine("Matrix1 3-rank minor:");
            Console.WriteLine(matrix1.GetMatrixMinor(3));

            // Let's get a supplementary minor to n-rank minor.
            Console.WriteLine("Supplementary minor to 3-rank minor:");
            Console.WriteLine(matrix1.GetSupplementaryMinor());

            // Let's get an element minor.
            Console.WriteLine("Element minor (2,3):");
            Console.WriteLine(matrix1.GetElementMinor(2, 3));

            // Wow. With minors that's it. Now let's check math operations.
            Console.WriteLine("Addition of matrix1 and matrix2:");
            Console.WriteLine((matrix1 + matrix2).ToString());

            Console.WriteLine("Subtraction of matrix1 and matrix2:");
            Console.WriteLine((matrix1 - matrix2).ToString());

            Console.WriteLine("Multiplication of matrix1 and matrix2:");
            Console.WriteLine((matrix1 * matrix2).ToString());

            Console.ReadKey();
        }
    }
}