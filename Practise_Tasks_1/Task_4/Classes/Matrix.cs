﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 * This is my Matrix class. Some examples of using Matrix you can find in Program.cs of this project.
 * 
 * Enjoy :)
 */

namespace FourthTask.Classes
{
    public class Matrix : IEnumerable
    {
        private double[,] _matrix;
        private int _rowCount;
        private int _columnCount;

        private int _currentRow;
        private List<int> _rowIndexes;
        private List<int> _columnIndexes;

        public int RowCount
        {
            get
            {
                return _rowCount;
            }

            private set
            {
                _rowCount = value;
            }
        }

        public int ColumnCount
        {
            get
            {
                return _columnCount;
            }

            private set
            {
                _columnCount = value;
            }
        }

        public double this[int row, int column]
        {
            get
            {
                if((row >= RowCount) || (row < 0))
                {
                    throw new IndexOutOfRangeException();
                }

                if((column >= ColumnCount) || (column < 0))
                {
                    throw new IndexOutOfRangeException();
                }

                return _matrix[row, column];
            }

            set
            {
                if ((row >= RowCount) || (row < 0))
                {
                    throw new IndexOutOfRangeException();
                }

                if ((column >= ColumnCount) || (column < 0))
                {
                    throw new IndexOutOfRangeException();
                }

                _matrix[row, column] = value;
            }
        }

        public Matrix(int rowCount, int columnCount)
        {
            RowCount = rowCount;
            ColumnCount = columnCount;

            _currentRow = 0;

            _matrix = new double[RowCount, ColumnCount];
        }

        public void Add(params double[] items)
        {
            if((items.Length > ColumnCount) || (_currentRow >= RowCount))
            {
                throw new IndexOutOfRangeException();
            }

            for(int i=0; i<items.Length; i++)
            {
                _matrix[_currentRow, i] = items[i];
            }

            _currentRow++;
        }

        public Matrix GetMatrixMinor(int rank)
        {
            if((rank > RowCount) || (rank > ColumnCount) || (rank < 1))
            {
                throw new ArgumentOutOfRangeException();
            }

            var minor = new Matrix(rank, rank);

            var random = new Random();
            _rowIndexes = new List<int>();
            _columnIndexes = new List<int>();

            var number = 0;

            for (var i = 0; i < rank; i++)
            {
                do
                {
                    number = random.Next(RowCount);
                } while (_rowIndexes.Contains(number));

                _rowIndexes.Add(number);

                do
                {
                    number = random.Next(ColumnCount);
                } while (_columnIndexes.Contains(number));

                _columnIndexes.Add(number);
            }

            _rowIndexes.Sort();
            _columnIndexes.Sort();

            var rowElements = new double[rank];

            for(var i=0; i<rank; i++)
            {
                rowElements = new double[rank];
                for (var j=0; j<rank; j++)
                {
                    rowElements[j] = _matrix[_rowIndexes[i], _columnIndexes[j]];
                }

                minor.Add(rowElements);
            }

            return minor;
        }

        public Matrix GetSupplementaryMinor()
        {
            if((_rowIndexes == null) || (_columnIndexes == null))
            {
                throw new NullReferenceException();
            }

            if(RowCount != ColumnCount)
            {
                throw new ArgumentException();
            }

            var rank = RowCount - _rowIndexes.Count;
            var minor = new Matrix(rank, rank);

            var fullIndexes = new List<int>();

            for(int i=0; i<= _rowIndexes.Max(); i++)
            {
                fullIndexes.Add(i);
            }

            var rowIndexes = fullIndexes.Except(_rowIndexes);
            fullIndexes.Clear();

            for (int i = 0; i <= _columnIndexes.Max(); i++)
            {
                fullIndexes.Add(i);
            }

            var columnIndexes = fullIndexes.Except(_columnIndexes);
            
            var rowElements = new double[rank];

            for (var i = 0; i < rowIndexes.Count(); i++)
            {
                rowElements = new double[rank];
                for (var j = 0; j < columnIndexes.Count(); j++)
                {
                    rowElements[j] = _matrix[rowIndexes.ElementAt(i), columnIndexes.ElementAt(j)];
                }

                minor.Add(rowElements);
            }

            return minor;
        }

        public Matrix GetElementMinor(int row, int column)
        {
            if (RowCount != ColumnCount)
            {
                throw new ArgumentException();
            }

            if((row > RowCount) || (row < 0))
            {
                throw new ArgumentOutOfRangeException();
            }

            if((column > ColumnCount) || (column < 0))
            {
                throw new ArgumentOutOfRangeException();
            }

            var rank = RowCount - 1;

            var minor = new Matrix(rank, rank);

            var rowElements = new double[rank];
            var elementIndex = 0;

            var flag = false;

            for(var i=0; i<RowCount; i++)
            {
                flag = false;
                rowElements = new double[rank];
                for (var j=0; j<ColumnCount; j++)
                {
                    if((i == row) || (j == column))
                    {
                        continue;
                    }

                    rowElements[elementIndex] = _matrix[i, j];
                    elementIndex++;
                    flag = true;
                }
                if (flag == true)
                {
                    minor.Add(rowElements);
                    elementIndex = 0;
                }
            }

            return minor;
        }

        public static Matrix operator+(Matrix matrix1, Matrix matrix2)
        {
            if((matrix1.RowCount != matrix2.RowCount) || (matrix2.ColumnCount != matrix2.ColumnCount))
            {
                throw new ArgumentException();
            }

            var resultMatrix = new Matrix(matrix1.RowCount, matrix1.ColumnCount);

            for(var i=0; i<matrix1.RowCount; i++)
            {
                for(var j=0; j<matrix1.ColumnCount; j++)
                {
                    resultMatrix[i, j] = matrix1[i, j] + matrix2[i, j];
                }
            }

            return resultMatrix;
        }

        public static Matrix operator -(Matrix matrix1, Matrix matrix2)
        {
            if ((matrix1.RowCount != matrix2.RowCount) || (matrix2.ColumnCount != matrix2.ColumnCount))
            {
                throw new ArgumentException();
            }

            var resultMatrix = new Matrix(matrix1.RowCount, matrix1.ColumnCount);

            for (var i = 0; i < matrix1.RowCount; i++)
            {
                for (var j = 0; j < matrix1.ColumnCount; j++)
                {
                    resultMatrix[i, j] = matrix1[i, j] - matrix2[i, j];
                }
            }

            return resultMatrix;
        }

        public static Matrix operator *(Matrix matrix1, Matrix matrix2)
        {
            if(matrix1.ColumnCount != matrix2.RowCount)
            {
                throw new ArgumentException();
            }

            var resultMatrix = new Matrix(matrix1.RowCount, matrix2.ColumnCount);

            for(var i=0; i<matrix1.RowCount; i++)
            {
                for(var j=0; j<matrix2.ColumnCount; j++)
                {
                    for(var k=0; k<matrix1.ColumnCount; k++)
                    {
                        resultMatrix[i, j] += matrix1[i,k] * matrix2[k,j];
                    }
                }
            }

            return resultMatrix;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            for(var i=0; i<RowCount; i++)
            {
                for(var j=0; j<ColumnCount; j++)
                {
                    builder.Append($"{_matrix[i,j]} ");
                }
                builder.Append("\n");
            }

            return builder.ToString();
        }

        public IEnumerator GetEnumerator()
        {
            for(var i=0; i<RowCount; i++)
            {
                for(var j=0; j<ColumnCount; j++)
                {
                    yield return _matrix[i, j];
                }
            }
        }
    }
}