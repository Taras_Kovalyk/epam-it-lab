﻿using SecondTask.Classes;
using System;

namespace SecondTask
{
    class Program
    {
        static void Main(string[] args)
        {
            // Let's create some rectangle with default values.
            var rectangle1 = new Rectangle();

            // Let's move this rectangle.
            rectangle1.X += 5;
            rectangle1.Y += 13;

            // Changing size of this rectangle.
            rectangle1.Width = 4;
            rectangle1.Height = 5;

            // Let's see what we got.
            Console.WriteLine($"First rectangle: {rectangle1.ToString()}");

            // Let's check the other functions of this class.
            var rectangle2 = new Rectangle(7, 11, 4, 6);
            Console.WriteLine($"Second rectangle: {rectangle2.ToString()}");

            Console.WriteLine("Rectangle, which contains first and second rectangle:");
            Console.WriteLine(Rectangle.GetRectangleWhichContains(rectangle1, rectangle2).ToString());

            Console.WriteLine("Rectangle, which is intersection of first and second rectangle:");
            Console.WriteLine(Rectangle.GetIntersectRectangle(rectangle1, rectangle2).ToString());

            Console.ReadKey();
        }
    }
}