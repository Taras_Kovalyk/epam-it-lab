﻿/*
 * This is my Rectangle class.
 * I describe a rectangle as first point on coordinates, width and height. 
 * Because if we know this information, we can easily find the other points of rectangle.
 * You can move rectangle and change size of rectangle by setting a new values to properties.
 * 
 * Overridden function ToString() was created to output all points of rectangle.
 * 
 * Some example of using Rectangle you can find in Program.cs of this project. Enjoy :)
 */

namespace SecondTask.Classes
{
    public class Rectangle
    {
        private int _x;
        private int _y;
        private int _width;
        private int _height;

        public int X
        {
            get
            {
                return _x;
            }

            set
            {
                _x = value;
            }
        }

        public int Y
        {
            get
            {
                return _y;
            }

            set
            {
                _y = value;
            }
        }

        public int Width
        {
            get
            {
                return _width;
            }

            set
            {
                _width = value;
            }
        }

        public int Height
        {
            get
            {
                return _height;
            }

            set
            {
                _height = value;
            }
        }

        public Rectangle()
        {
            X = 0;
            Y = 0;
            Width = 0;
            Height = 0;
        }

        public Rectangle(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public static Rectangle GetRectangleWhichContains(Rectangle rectangle1, Rectangle rectangle2)
        {
            var resultRectangle = new Rectangle();

            resultRectangle.X = rectangle1.X <= rectangle2.X ? rectangle1.X : rectangle2.X;
            resultRectangle.Y = rectangle1.Y >= rectangle2.Y ? rectangle1.Y : rectangle2.Y;

            if((rectangle1.X + rectangle1.Width) >= (rectangle2.X + rectangle2.Width))
            {
                resultRectangle.Width = (rectangle1.X + rectangle1.Width) - resultRectangle.X;
            }
            else
            {
                resultRectangle.Width = (rectangle2.X + rectangle2.Width) - resultRectangle.X;
            }

            if((rectangle1.Y - rectangle1.Height) >= (rectangle2.Y - rectangle2.Height))
            {
                resultRectangle.Height = resultRectangle.Y - (rectangle2.Y - rectangle2.Height);
            }
            else
            {
                resultRectangle.Height = resultRectangle.Y - (rectangle1.Y - rectangle1.Height);
            }

            return resultRectangle;
        }

        public static Rectangle GetIntersectRectangle(Rectangle rectangle1, Rectangle rectangle2)
        {
            var resultRectangle = new Rectangle();

            resultRectangle.X = rectangle1.X >= rectangle2.X ? rectangle1.X : rectangle2.X;
            resultRectangle.Y = rectangle1.Y <= rectangle2.Y ? rectangle1.Y : rectangle2.Y;

            if ((rectangle1.X + rectangle1.Width) <= (rectangle2.X + rectangle2.Width))
            {
                resultRectangle.Width = (rectangle1.X + rectangle1.Width) - resultRectangle.X;
            }
            else
            {
                resultRectangle.Width = (rectangle2.X + rectangle2.Width) - resultRectangle.X;
            }

            if ((rectangle1.Y - rectangle1.Height) <= (rectangle2.Y - rectangle2.Height))
            {
                resultRectangle.Height = resultRectangle.Y - (rectangle2.Y - rectangle2.Height);
            }
            else
            {
                resultRectangle.Height = resultRectangle.Y - (rectangle1.Y - rectangle1.Height);
            }

            return resultRectangle;
        }

        public override string ToString()
        {
            return $"A({X},{Y}); B({X+Width},{Y}); C({X+Width},{Y-Height}); D({X},{Y-Height}); \n";
        }
    }
}