﻿using System;
using System.Text;

/*
 * This is my Polynomial class. Some examples of using Polynomial you can find in Program.cs of this project.
 * P.S. Degree - it's the biggest value of polynomial degrees. (coefficients.Length - 1)
 * 
 * Enjoy :)
 */

namespace ThirdTask.Classes
{
    public class Polynomial
    {
        private int[] _coefficients;
        private int _degree;

        public int Degree
        {
            get
            {
                return _degree;
            }

            private set
            {
                _degree = value;
            }
        }

        public int this[int index]
        {
            get
            {
                if((index < 0) || (index > Degree))
                {
                    throw new IndexOutOfRangeException();
                }

                return _coefficients[index];
            }
            set
            {
                if ((index < 0) || (index > Degree))
                {
                    throw new IndexOutOfRangeException();
                }

                _coefficients[index] = value;
            }
        }

        public Polynomial(int[] coefficients)
        {
            Degree = coefficients.Length - 1; ;
            _coefficients = new int[Degree+1];

            for(var i = 0; i <= Degree; i++)
            {
                _coefficients[i] = coefficients[i];
            }
        }

        public int Calculate(int argument)
        {
            var value = 0;

            for(var i = 0; i <= Degree; i++)
            {
                value += _coefficients[i] * (Convert.ToInt32(Math.Pow(argument, i)));
            }

            return value;
        }

        public static Polynomial operator +(Polynomial polynomial1, Polynomial polynomial2)
        {
            int degree = Math.Max(polynomial1.Degree, polynomial2.Degree);
            var resultCoefficients = new int[degree + 1];

            if(polynomial1.Degree > polynomial2.Degree)
            {
                for(var i = 0; i <= polynomial2.Degree; i++)
                {
                    resultCoefficients[i] = polynomial1[i] + polynomial2[i];
                }

                for(int i = polynomial2.Degree+1; i <= polynomial1.Degree; i++)
                {
                    resultCoefficients[i] = polynomial1[i];
                }
            }

            else if (polynomial1.Degree < polynomial2.Degree)
            {
                for (var i = 0; i <= polynomial1.Degree; i++)
                {
                    resultCoefficients[i] = polynomial1[i] + polynomial2[i];
                }

                for (int i = polynomial1.Degree + 1; i <= polynomial2.Degree; i++)
                {
                    resultCoefficients[i] = polynomial2[i];
                }
            }

            else
            {
                for(int i = 0; i < polynomial1.Degree; i++)
                {
                    resultCoefficients[i] = polynomial1[i] + polynomial2[i];
                }
            }

            return new Polynomial(resultCoefficients);
        }

        public static Polynomial operator -(Polynomial polynomial1, Polynomial polynomial2)
        {
            int degree = Math.Max(polynomial1.Degree, polynomial2.Degree);
            var resultCoefficients = new int[degree + 1];

            if (polynomial1.Degree > polynomial2.Degree)
            {
                for (var i = 0; i <= polynomial2.Degree; i++)
                {
                    resultCoefficients[i] = polynomial1[i] - polynomial2[i];
                }

                for (int i = polynomial2.Degree + 1; i <= polynomial1.Degree; i++)
                {
                    resultCoefficients[i] = -polynomial1[i];
                }
            }

            else if (polynomial1.Degree < polynomial2.Degree)
            {
                for (var i = 0; i <= polynomial1.Degree; i++)
                {
                    resultCoefficients[i] = polynomial1[i] - polynomial2[i];
                }

                for (int i = polynomial1.Degree + 1; i <= polynomial2.Degree; i++)
                {
                    resultCoefficients[i] = -polynomial2[i];
                }
            }

            else
            {
                for (int i = 0; i < polynomial1.Degree; i++)
                {
                    resultCoefficients[i] = polynomial1[i] - polynomial2[i];
                }
            }

            return new Polynomial(resultCoefficients);
        }

        public static Polynomial operator *(Polynomial polynomial1, Polynomial polynomial2)
        {
            int degree = polynomial1.Degree + polynomial2.Degree;
            var resultCoefficients = new int[degree + 1];

            for(var i = 0; i <= polynomial1.Degree; i++)
            {
                for(var j = 0; j <= polynomial2.Degree; j++)
                {
                    resultCoefficients[i + j] += polynomial1[i] * polynomial2[j];
                }
            }

            return new Polynomial(resultCoefficients);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            for(var i = 0; i <= Degree; i++)
            {
                builder.Append($"({_coefficients[i]}x^{i})");
                
                if(i!=Degree)
                {
                    builder.Append(" + ");
                }
            }

            return builder.ToString();
        }
    }
}